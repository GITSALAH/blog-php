-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : sam. 16 mai 2020 à 18:10
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `published` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `topic_id`, `title`, `image`, `body`, `published`, `created_at`) VALUES
(19, 11, 7, 'Essai Audi A5 Sportback 2020, la plus latine des Allemandes !', '1589633777_audi-a5-sportback_2020-Sline-V6-tdi-960x500.jpg', '&lt;p&gt;C&rsquo;est plus pr&eacute;cis&eacute;ment &agrave; Anvers que je pars essayer cette tr&egrave;s belle berline coup&eacute;. Audi sait faire de belles autos, et depuis les premi&egrave;res g&eacute;n&eacute;rations, l&rsquo;Audi A5 sortait du lot. Contrairement &agrave; l&rsquo;Audi A4 qui donne cet effet tr&egrave;s classique d&rsquo;une berline routi&egrave;re, la A5 en finition Sline donne plus d&rsquo;&eacute;l&eacute;gance et de sportivit&eacute;. Sous ses airs latins, se cache un coup&eacute; 5 portes tant&ocirc;t sportif et tant&ocirc;t pratique. Dans les faits, le charme op&egrave;re avec cette sublime teinte Vert Distric m&eacute;tallis&eacute;e qui fait tourner toutes les t&ecirc;tes et ce m&ecirc;me malgr&eacute; le claquement timide du diesel. Dans les rues atypiques d&rsquo;Anvers, mon Audi A5 camp&eacute;e sur ses grosses jantes de 20 pouces fait clairement de l&rsquo;effet. Je suis un homme heureux &agrave; son volant, fier de rouler avec la toute derni&egrave;re monture restylis&eacute;e pour l&rsquo;occasion. Le restylage est certes l&eacute;ger, quelques &eacute;l&eacute;ments esth&eacute;tiques changent comme ses optiques avant et arri&egrave;re &agrave; LED, un d&eacute;tail qui change tout.&amp;nbsp;&lt;/p&gt;', 1, '2020-05-16 12:56:17'),
(20, 11, 7, 'La dernière des Focus RS ?', '1589633838_ford-focus-rs-Mk3-nitro-960x500.jpg', '&lt;p&gt;Essai de la derni&egrave;re des&lt;a href=&quot;https://www.leblog-carspassion.fr/essais-auto/avis-essai-ford-focus-rs-pack-performance-mk3-350&quot;&gt; Ford Focus RS MK3&lt;/a&gt; Pack Performance, la moins ch&egrave;re des sportives est capable de rivaliser contre une Renault M&eacute;gane 4 RS, une Audi RS3, une Vw Golf 7 R et une Mercedes A45 AMG.&lt;/p&gt;', 1, '2020-05-16 12:57:18'),
(21, 11, 7, 'Lamborghini Urus, une bête sauvage !', '1589633922_lamborghini-urus-off-road-960x500.jpg', '&lt;h3&gt;&lt;strong&gt;Une Lamborghini en terre inconnue&amp;nbsp;&lt;/strong&gt;&lt;/h3&gt;&lt;p&gt;L&rsquo;essai du Lamborghini Urus se d&eacute;roule en terre&amp;nbsp;portugaise&amp;nbsp;sur les c&ocirc;tes de&amp;nbsp;Nazar&eacute;&amp;nbsp;&agrave; une heure de route de Lisbonne.&amp;nbsp;Un coin tr&egrave;s c&eacute;l&egrave;bre pour les surfeurs puisque chaque ann&eacute;e vers la fin du mois d&rsquo;octobre, on observe&lt;strong&gt; les plus grandes vagues du&amp;nbsp;monde.&lt;/strong&gt;&amp;nbsp;Certaines peuvent m&ecirc;me atteindre plus de 30 m&egrave;tres de hauteur.&amp;nbsp;C&rsquo;est donc dans cet univers d&eacute;contract&eacute;, entre planches de surf et jolis paysages que je vais essayer et &eacute;prouver toute la polyvalence du nouveau et puissant Lamborghini Urus, du moins c&rsquo;est la promesse faite par Pirelli et Lamborghini.&amp;nbsp;&lt;/p&gt;', 1, '2020-05-16 12:58:42'),
(22, 11, 7, 'Audi Q8 50 TDI, l’essai d’un SUV Coupé à la pointe de la technologie', '1589634003_audi-q8-tdi-50-2020-essai-prix-960x500.jpg', '&lt;h3&gt;&lt;strong&gt;Un corps de SUV muscl&eacute; dans un coup&eacute;&lt;/strong&gt;&lt;/h3&gt;&lt;p&gt;Audi frappe tr&egrave;s fort en lan&ccedil;ant &lt;a href=&quot;https://www.leblog-carspassion.fr/fiches-techniques/mercedes-gle-coupe-63-s-amg-fiche-technique-performances-prix&quot;&gt;ce beau SUV coup&eacute;&lt;/a&gt; sur le march&eacute;. Il vient clairement concurrencer les BMW X6 et autres Mercedes GLE Coup&eacute;. A c&ocirc;t&eacute; l&rsquo;Audi Q8 fait plus muscl&eacute; que ces deux autres rivaux. Avec sa calandre Single Frame imposante, ses ailes &eacute;largies, ses passages de roue qui accueillent d&rsquo;&eacute;normes jantes de 22 pouces, l&rsquo;Audi Q8 en impose avec ses 5 m&egrave;tres de long et 2 m&egrave;tres de large. Sa ligne de toit fuyante propre au Coup&eacute; &eacute;tonne sur ce bel engin massif.&lt;/p&gt;', 1, '2020-05-16 13:00:03'),
(23, 11, 8, 'OCP: Une deuxième école YouCode a ouvert ses portes à Safi', '1589634085_IMG-20191127-WA00071.jpg', '&lt;p&gt;Suite &agrave; la r&eacute;ussite de l&rsquo;exp&eacute;rience &quot;YouCode&quot; &agrave; El Youssoufia, ouverte en octobre 2018 et qui accueille aujourd&rsquo;hui environ 200 &eacute;tudiants, OCP a ouvert une&amp;nbsp;deuxi&egrave;me &eacute;cole &agrave; Safi en octobre dernier, en partenariat avec &lt;a href=&quot;https://simplon.co/&quot;&gt;Simplon&lt;/a&gt;. Cette derni&egrave;re est une entreprise sociale et solidaire offrant des formations li&eacute;es aux m&eacute;tiers du num&eacute;rique.&lt;/p&gt;&lt;p&gt;L&rsquo;&eacute;cole &quot;YouCode&quot; offre une formation gratuite qui s&rsquo;&eacute;tale sur deux ann&eacute;es. La premi&egrave;re est consacr&eacute;e principalement aux fondamentaux du d&eacute;veloppement informatique mais offre aussi des rencontres avec les entreprises. La deuxi&egrave;me&amp;nbsp;est d&eacute;di&eacute;e &agrave; la sp&eacute;cialisation des &eacute;tudiants dans des technologies de pointe.&amp;nbsp;&lt;/p&gt;', 1, '2020-05-16 13:01:25');

-- --------------------------------------------------------

--
-- Structure de la table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `topics`
--

INSERT INTO `topics` (`id`, `name`, `description`) VALUES
(3, 'Philosophie', '<p>yeaheee</p>'),
(4, 'Quotes', '<p>test</p>'),
(5, 'Fiction', '<p>test</p>'),
(6, 'Biography', '<p>test</p>'),
(7, 'Motivation', '<p>Test</p>'),
(8, 'Inspirations', '<p><strong>test</strong></p>');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `admin` tinyint(4) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `admin`, `username`, `email`, `password`, `created_at`) VALUES
(11, 1, 'GITSALAH', 'salah.bouanba2@gmail.com', '$2y$10$LDGCuY68kHiWa3qYOeat2.raHQXGWHbcbfcixpTs9/jZTwHw58WZW', '2020-05-16 12:36:16');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topic_id` (`topic_id`);

--
-- Index pour la table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
